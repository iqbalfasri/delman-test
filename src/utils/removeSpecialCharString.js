const removeSpecialChar = (string) => {
  return string.replace(/[_-]/g, " ");
};

export default removeSpecialChar;
