import React from "react";
import { useTable, useBlockLayout, useGlobalFilter } from "react-table";
import { FixedSizeList } from "react-window";
import {
  Box,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Checkbox,
  CheckboxGroup,
} from "@chakra-ui/react";

import { SearchComponent } from "../";
import { removeSpecialChar } from "../../utils";

const TableComponent = ({
  columns,
  data,
  allColumns,
  onScrollVertical,
  onScrollHorizontal,
  refScrollHorizontal,
  refScrollVertical,
  withViewButton,
}) => {
  // modal state
  const { isOpen, onOpen, onClose } = useDisclosure();

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state,
    preGlobalFilteredRows,
    setGlobalFilter,
    totalColumnsWidth,
    allColumns: allTableColumns,
  } = useTable(
    {
      columns,
      data,
    },
    useBlockLayout,
    useGlobalFilter
  );

  React.useEffect(() => {
    if (allTableColumns && allColumns) {
      allColumns(allTableColumns);
    }
  }, [allColumns, allTableColumns]);

  const RenderRow = React.useCallback(
    ({ index, style }) => {
      const row = rows[index];
      prepareRow(row);
      return (
        <Box
          {...row.getRowProps({
            style,
          })}
          className="tr"
        >
          {row.cells.map((cell, index) => {
            return (
              <Box key={index} {...cell.getCellProps()}>
                {cell.render("Cell")}
              </Box>
            );
          })}
        </Box>
      );
    },
    [prepareRow, rows]
  );

  return (
    <Box>
      {/* Hide column */}
      {withViewButton ? (
        <Box mb="12">
          <Button colorScheme="facebook" onClick={onOpen}>
            View
          </Button>
        </Box>
      ) : null}

      {/* Modal */}
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Hide Column</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Box display="flex" flexDirection="column">
              {allTableColumns.length > 0 &&
                allTableColumns.map((col) => {
                  const { checked } = col.getToggleHiddenProps();
                  return (
                    <Checkbox
                      key={col.id}
                      isChecked={checked}
                      {...col.getToggleHiddenProps()}
                    >
                      {removeSpecialChar(col.id)}
                    </Checkbox>
                  );
                })}
            </Box>
          </ModalBody>
        </ModalContent>
      </Modal>

      {/* Search */}
      <Box mb="12">
        <SearchComponent
          preGlobalFilteredRows={preGlobalFilteredRows}
          globalFilter={state.globalFilter}
          setGlobalFilter={setGlobalFilter}
        />
      </Box>

      <Box
        ref={refScrollHorizontal}
        onScroll={onScrollHorizontal}
        overflowX="auto"
      >
        <Box {...getTableProps()}>
          <Box>
            {headerGroups.map((headerGroup, index) => (
              <Box
                bg="teal.400"
                display="flex"
                alignItems="center"
                justifyContent="center"
                key={index}
                {...headerGroup.getHeaderGroupProps()}
              >
                {headerGroup.headers.map((column) => (
                  <Box key={index} {...column.getHeaderProps()}>
                    {column.render("Header")}
                  </Box>
                ))}
              </Box>
            ))}
          </Box>
          <Box {...getTableBodyProps()}>
            <FixedSizeList
              outerRef={refScrollVertical}
              onScroll={onScrollVertical}
              height={400}
              itemCount={rows.length}
              itemSize={35}
              width={totalColumnsWidth}
            >
              {RenderRow}
            </FixedSizeList>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default React.memo(TableComponent);
