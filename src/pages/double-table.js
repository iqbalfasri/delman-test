import React from "react";
import { Box, Container, Flex, Text } from "@chakra-ui/react";

import {
  FileUploadComponent as FileUpload,
  TableComponent as Table,
} from "../components";

import { removeSpecialChar } from "../utils";

const DoubleTable = () => {
  const [rawData, setRawData] = React.useState([]);
  const [column, setColumn] = React.useState([]);
  const [data, setData] = React.useState([]);

  const [rawDataRigt, setRawDataRight] = React.useState([]);
  const [columnRight, setColumnRight] = React.useState([]);
  const [dataRight, setDataRight] = React.useState([]);

  const handleColumn = React.useCallback((data, position = "left") => {
    if (position == "left") {
      setRawData(data);
    } else if (position === "right") {
      setRawDataRight(data);
    }
  }, []);

  // Left Data
  React.useEffect(() => {
    if (rawData.length > 0) {
      const arrColumn = [];
      const getObjKeys = Object.keys(rawData[0]);

      for (const key of getObjKeys) {
        if (key.trim() !== "") {
          const removeUnderscore = removeSpecialChar(key);
          const newObj = {
            Header: removeUnderscore,
            accessor: key,
          };

          arrColumn.push(newObj);
        }
      }

      setColumn(arrColumn);
    }
  }, [rawData]);
  React.useEffect(() => {
    if (rawData.length > 0) {
      const arrData = [];

      for (const data of rawData) {
        delete data[""]; //remove "" key object
        arrData.push({ ...data });
      }

      setData(arrData);
    }
  }, [rawData]);

  // RightData
  React.useEffect(() => {
    if (rawDataRigt.length > 0) {
      const arrColumn = [];
      const getObjKeys = Object.keys(rawDataRigt[0]);

      for (const key of getObjKeys) {
        if (key.trim() !== "") {
          const removeUnderscore = removeSpecialChar(key);
          const newObj = {
            Header: removeUnderscore,
            accessor: key,
          };

          arrColumn.push(newObj);
        }
      }

      setColumnRight(arrColumn);
    }
  }, [rawDataRigt]);
  React.useEffect(() => {
    if (rawDataRigt.length > 0) {
      const arrData = [];

      for (const data of rawDataRigt) {
        delete data[""]; //remove "" key object
        arrData.push({ ...data });
      }

      setDataRight(arrData);
    }
  }, [rawDataRigt]);

  const leftRef = React.useRef();
  const rightRef = React.useRef();
  const leftRefVertical = React.useRef();
  const rightRefVertical = React.useRef();

  const onLeftScroll = (scroll) => {
    if (rightRef.current) {
      rightRef.current.scrollLeft = scroll.target.scrollLeft;
    }
  };
  const onRightScroll = (scroll) => {
    if (leftRef.current) {
      leftRef.current.scrollLeft = scroll.target.scrollLeft;
    }
  };

  const onLeftScrollVertical = (scroll) => {
    if (rightRefVertical?.current) {
      console.log(rightRefVertical?.current?.scrollTop, "vertical scroll");
      rightRefVertical.current.scrollTop = scroll.scrollOffset;
    }
  };
  const onRightScrollVertical = (scroll) => {
    if (leftRefVertical?.current) {
      console.log(leftRefVertical?.current?.scrollTop, "right vertical scroll");
      leftRefVertical.current.scrollTop = scroll.scrollOffset;
    }
  };

  return (
    <Box py="48">
      <Container minW="full">
        <Flex
          flexDirection="row"
          alignItems="center"
          justifyContent="space-between"
        >
          <Box display="flex" flexDirection="column" flex={1}>
            <Box mb="12">
              <FileUpload data={(result) => handleColumn(result)} />
            </Box>
            <Box maxW="md">
              {column.length > 0 && data.length > 0 ? (
                <Table
                  columns={column}
                  data={data}
                  refScrollHorizontal={leftRef}
                  refScrollVertical={leftRefVertical}
                  onScrollHorizontal={onLeftScroll}
                  onScrollVertical={onLeftScrollVertical}
                  withViewButton={false}
                />
              ) : (
                <Text>Mohon upload file terlebih dahulu</Text>
              )}
            </Box>
          </Box>

          <Box display="flex" flexDirection="column" flex={1}>
            <Box mb="12">
              <FileUpload data={(result) => handleColumn(result, "right")} />
            </Box>
            <Box maxW="md">
              {columnRight.length > 0 && dataRight.length > 0 ? (
                <Table
                  columns={columnRight}
                  data={dataRight}
                  refScrollHorizontal={rightRef}
                  refScrollVertical={rightRefVertical}
                  onScrollHorizontal={onRightScroll}
                  onScrollVertical={onRightScrollVertical}
                  withViewButton={false}
                />
              ) : (
                <Text>Mohon upload file terlebih dahulu</Text>
              )}
            </Box>
          </Box>
        </Flex>
      </Container>
    </Box>
  );
};

export default DoubleTable;
