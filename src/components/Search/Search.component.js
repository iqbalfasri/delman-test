import React from "react";
import { Input } from "@chakra-ui/react";
import { useAsyncDebounce } from "react-table";

const SearchComponent = ({
  preGlobalFilteredRows,
  globalFilter,
  setGlobalFilter,
}) => {
  const [searchValue, setSearchValue] = React.useState(globalFilter);
  const onChange = useAsyncDebounce((value) => {
    setGlobalFilter(value || undefined);
  }, 200);

  return (
    <Input
      placeholder={`Search ${preGlobalFilteredRows.length} records...`}
      value={searchValue || ""}
      onChange={({ target }) => {
        setSearchValue(target.value);
        onChange(target.value);
      }}
    />
  );
};

export default React.memo(SearchComponent);
