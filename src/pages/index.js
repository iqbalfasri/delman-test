import * as React from "react";

import {
  Container,
  Box,
  Text,
  Checkbox,
  Spinner,
  Button,
} from "@chakra-ui/react";

import {
  TableComponent as Table,
  FileUploadComponent as FileUpload,
} from "../components";

import { removeSpecialChar } from "../utils";
import Link from "next/link";

export default function Home() {
  const [rawData, setRawData] = React.useState([]);
  const [column, setColumn] = React.useState([]);
  const [data, setData] = React.useState([]);

  const handleColumn = React.useCallback((data) => {
    setRawData(data);
  }, []);

  React.useEffect(() => {
    if (rawData.length > 0) {
      const arrColumn = [];
      const getObjKeys = Object.keys(rawData[0]);

      for (const key of getObjKeys) {
        if (key.trim() !== "") {
          const removeUnderscore = removeSpecialChar(key);
          const newObj = {
            Header: removeUnderscore,
            accessor: key,
          };

          arrColumn.push(newObj);
        }
      }

      setColumn(arrColumn);
    }
  }, [rawData]);

  React.useEffect(() => {
    if (rawData.length > 0) {
      const arrData = [];

      for (const data of rawData) {
        delete data[""]; //remove "" key object
        arrData.push({ ...data });
      }

      setData(arrData);
    }
  }, [rawData]);

  return (
    <Box py="48">
      <Container maxW="container.lg" mb="14">
        <FileUpload data={(result) => handleColumn(result)} />

        <Box mt="4">
          <Link passHref href="/double-table">
            <Button as="a" colorScheme="green">
              Go to double-table page
            </Button>
          </Link>
        </Box>
      </Container>

      <Box display="flex" flex={1} justifyContent="center">
        <Box maxW="container.lg">
          {column.length > 0 && data.length > 0 ? (
            <Table columns={column} data={data} withViewButton={true} />
          ) : (
            <Text>Mohon upload file terlebih dahulu</Text>
          )}
        </Box>
      </Box>
    </Box>
  );
}
