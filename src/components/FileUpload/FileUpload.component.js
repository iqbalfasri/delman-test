import * as React from "react";
import { Input, Button } from "@chakra-ui/react";
import { useCSVReader } from "react-papaparse";

const FileUploadComponent = ({ data }) => {
  const { CSVReader } = useCSVReader();

  return (
    <CSVReader
      onUploadAccepted={(result) => {
        data(result.data.flatMap((r) => r));
      }}
      config={{
        header: true,
      }}
    >
      {({ getRootProps, acceptedFile, ProgressBar, getRemoveFileProps }) => {
        return (
          <>
            <form>
              <Button {...getRootProps()}>Browse file</Button>
            </form>
            <ProgressBar style={{ backgroundColor: "red" }} />
          </>
        );
      }}
    </CSVReader>
  );
};

export default FileUploadComponent;
